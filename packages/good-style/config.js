export default {
	listType: 1, //商品样式
	cart: 1, //加购物车样式
	badge_id: 0, //角标
	badge_path: '', //选中的角标路径
	customBadge: '', //自定义角标
	isShowCate: 1, //是否展示快捷进入分类
	isShowBuyHis: 1, //是否显示购买记录
}
