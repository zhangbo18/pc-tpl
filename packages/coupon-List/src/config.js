let editList = {
	TemplateTypeList: [
		{
			icon: 'el-icon-eleme',
			value: 1,
			label: '样式1',
		},
		{
			icon: 'el-icon-eleme',
			value: 2,
			label: '样式2',
		},
		{
			icon: 'el-icon-eleme',
			value: 3,
			label: '样式3',
		},
		{
			icon: 'el-icon-eleme',
			value: 4,
			label: '样式4',
		},
		{
			icon: 'el-icon-eleme',
			value: 5,
			label: '样式5',
		},
	],
	couponWayList: [
		{
			icon: 'el-icon-eleme',
			value: 1,
			label: '横向滑动',
		},
		{
			icon: 'el-icon-eleme',
			value: 2,
			label: '一行两个',
		},
	],
	couponStatusList: [
		{
			icon: 'el-icon-eleme',
			value: 1,
			label: '未领取',
		},
		{
			icon: 'el-icon-eleme',
			value: 2,
			label: '已领取',
		},
		{
			icon: 'el-icon-eleme',
			value: 3,
			label: '已失效',
		},
	],
}
export default editList
